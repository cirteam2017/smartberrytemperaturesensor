#include "RFLibrary.h"

Packet* packetToSend(){
  float temp = readTemperature();
  Packet* packet = calloc(1,sizeof(Packet));
  packet->content = calloc(10,sizeof(uint8_t));
  packet->content[0] = 0x0;
  packet->content[1] = 10;
  packet->content[4] = 0x02;
  packet->content[5] = 0x10;
  packet->content[6] = (uint8_t) temp;
  packet->content[7] = (int)((temp - ((int) temp))*100);
  packet->content[9] = 0x03;
  packet->contentLength = 10;
  return packet;
  }

void packetReceived(Packet packet){
  
  }

RFLibrary rfLibrary = RFLibrary(1000,false,packetToSend,packetReceived);
void setup() {
  rfLibrary.setupRFModule(3,2,1,1);
  
}

float readTemperature(){
  int val = analogRead(1);
  float mv = ( val/1024.0)*5000; 
  float cel = mv/10;
  float farh = (cel*9)/5 + 32;
  
  Serial.print("TEMPRATURE = ");
  Serial.print(cel);
  Serial.print("*C");
  Serial.println();
  return cel;
  
}

void loop() {
  rfLibrary.startLoop();

}
